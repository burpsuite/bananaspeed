<?php

class BananaSpeed
{

    /**
     * 获取公钥解密结果
     * @param $EnCryptData
     * @return mixed
     */
    public function getDeCryptResult($EnCryptData)
    {
        $PublicKey = openssl_pkey_get_public(file_get_contents('PublicKey'));
        openssl_public_decrypt(base64_decode($EnCryptData),$DeCryptResult,$PublicKey);
        return $DeCryptResult;
    }

    /**
     * 获取BananaService列表数量
     * @param $File
     * @param $NodesCount
     * @return array
     */
    public function getListCount($File,$NodesCount)
    {
        $ListCount = NULL;
        for($i=0;$i<$NodesCount;++$i)
        {
            $ListCount[] = count($File->result->nodes[$i]->list);
        }
        return $ListCount;
    }


    /**
     * PHP cURL请求模块
     * @param $RequestInfo
     * @return mixed
     */
    public function cURL($RequestInfo)
    {
        if(!empty($RequestInfo))
        {
            $cURL = curl_init();
            curl_setopt($cURL,CURLOPT_URL,$RequestInfo['Address']);
            curl_setopt($cURL,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($cURL,CURLOPT_USERAGENT,$RequestInfo['UserAgent']);
            curl_setopt($cURL,CURLOPT_TIMEOUT,30);
            curl_setopt($cURL,CURLOPT_REFERER,$RequestInfo['Address']);
            curl_setopt($cURL,CURLOPT_COOKIE,$RequestInfo['Cookies']);
            $cURLContent = curl_exec($cURL);
            curl_close($cURL);
            return $cURLContent;
        }else{
            return exit('Request Error!');
        }
    }

}

?>